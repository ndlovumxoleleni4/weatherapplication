const apiKey = '566f47e6fab7d412c46549b487c29fc1';
const searchForm = document.querySelector('#search-form');
const searchInput = searchForm.querySelector('input');
const currentWeather = document.querySelector('#current-weather');
const forecast = document.querySelector('#forecast');

searchForm.addEventListener('submit', async (e) => {
    e.preventDefault();
    const searchTerm = searchInput.value;
    try {
        await getCurrentWeather(searchTerm);
        await getForecast(searchTerm);
    } catch (err) {
        console.error(err);
        alert("An error occurred while trying to fetch weather data. Please try again later.");
    }
});

async function getCurrentWeather(location) {
    try {
        const res = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${location}&appid=${apiKey}`);
        const data = await res.json();
        const { name, main: { temp, temp_min, temp_max } } = data;
        const weather = data.weather[0].main;
        currentWeather.innerHTML = `
        <div>Location: ${name}</div>
        <div>Weather: ${weather}</div>
        <div>Temperature: ${(temp - 273.15).toFixed(2)} &#8451;</div>
        <div>Min. Temperature: ${(temp_min - 273.15).toFixed(2)} &#8451;</div>
        <div>Max. Temperature: ${(temp_max - 273.15).toFixed(2)} &#8451;</div>
        `;
    } catch (err) {
        throw err;
    }
}

async function getForecast(location) {
    try {
        const res = await fetch(`https://api.openweathermap.org/data/2.5/forecast?q=${location}&appid=${apiKey}`);
        const data = await res.json();
        forecast.innerHTML = '';
        for (let i = 0; i < data.list.length; i += 8) {
            const { dt_txt, main: { temp, temp_min, temp_max } } = data.list[i];
            const date = new Date(dt_txt);
            forecast.innerHTML += `
            <li>
                <div>Date: ${date.toLocaleString()}</div>
                <div>Min. Temp: ${(temp_min - 273.15).toFixed(2)} &#8451;</div>
                <div>Max. Temp: ${(temp_max - 273.15).toFixed(2)} &#8451;</div>
                <div>Temp: ${(temp - 273.15).toFixed(2)} &#8451;</div>
            </li>
            `;
        }
    } catch (err) {
        throw err;
    }
}
