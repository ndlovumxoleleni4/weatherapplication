# Weather App

This is a simple weather application that allows users to check the current weather conditions and forecast for their location.

## Getting Started

1. Clone the repository
2. Get an API key from a weather service provider such as OpenWeatherMap, Weather Underground, or Dark Sky.
3. Replace "YOUR_API_KEY" with your actual API key in the app.js file.
4. Open the index.html file in your browser.

## Built With

- HTML
- CSS
- JavaScript

## Details
This JavaScript code is using the Fetch API to retrieve data from the OpenWeatherMap API. It is also using the DOM API to update the page with the retrieved data.

The code first defines a few variables that reference elements on the page, such as the search form, current weather, and forecast elements. Then, it adds an event listener to the search form that listens for the submit event. When the event is fired, the event listener calls two asynchronous functions: getCurrentWeather and getForecast. These functions use the Fetch API to retrieve the current weather and forecast data from the OpenWeatherMap API, respectively.

getCurrentWeather function takes location as input and retrieve the current weather data from the OpenWeatherMap API by making a GET request to the following url https://api.openweathermap.org/data/2.5/weather?q=${location}&appid=${apiKey}

getForecast function also takes location as input and retrieve the forecast data from the OpenWeatherMap API by making a GET request to the following url https://api.openweathermap.org/data/2.5/forecast?q=${location}&appid=${apiKey}

Once the data is retrieved, it is parsed into JSON format and then used to update the current weather and forecast elements on the page by updating their innerHTML properties.

The CSS styles the different elements on the page, such as the container, search form, current weather, and forecast elements. It sets various properties such as font size, padding, margin, background color, and border radius.

## Authors

- Mxoleleni Ndlovu

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
